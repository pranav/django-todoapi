from rest_framework import serializers
from todoapi.models import Task
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    tasks = serializers.PrimaryKeyRelatedField(many=True, queryset=Task.objects.all())

    class Meta:
        model = User
        fields = ["id", "username", "tasks", "url"]


class TaskSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source="owner.username")

    class Meta:
        model = Task
        fields = ["title", "due", "done", "owner", "url"]
