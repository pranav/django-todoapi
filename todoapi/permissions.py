from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to access it.
    """

    def has_object_permission(self, request, view, obj):
        # print("permission")
        # print("obj", obj)
        # print("obj.owner", obj.owner, "request.user", request.user)
        # Permissions are only allowed to the owner of the task
        return obj.owner == request.user
